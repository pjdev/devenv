# devenv

Macbook Automate Install Script

## Automate Script Installation Step:

* On a new Mac, open Terminal app and execute the script below:

```
mkdir ~/.devfor && cd ~/.devfor

git clone https://gitlab.com/pjdev/devenv.git

bash <(curl -s https://gitlab.com/pjdev/devfor/raw/master/scripts/init)
```

If you're prompted to install Development Scripts from Apple, click on `Install`

## Manual App Store install:

* [x] XCode

ҟ