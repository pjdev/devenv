#!/bin/bash

ITERM="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

link_plist() {
    # Specify the preferences directory
    defaults write com.googlecode.iterm2.plist PrefsCustomFolder -string "$ITERM/iterm"
    # Tell iTerm2 to use the custom preferences in the directory
    defaults write com.googlecode.iterm2.plist LoadPrefsFromCustomFolder -bool true
}

install_iterm_fonts() {
    local fonts_dir=$HOME/.devfor/fonts
    # Setup powerline fonts
    if [ ! -f $fonts_dir/install.sh ]; then
        git clone https://github.com/powerline/fonts.git --depth=1 $fonts_dir
        sh $fonts_dir/install.sh
    fi
}

setup_iterm() {
    link_plist
    install_iterm_fonts
}

setup_iterm