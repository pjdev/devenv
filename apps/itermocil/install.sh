#!/bin/bash

ITERMOCIL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

link_itermocil() {
  local dest_dir=$HOME/.itermocil

  if [ ! -d $dest_dir ]; then
    mkdir -p $dest_dir
  fi

  find $ITERMOCIL -type f -name \*.yml | xargs -I {} ln -s -f "{}" $dest_dir
  echo "Symlinked all itermocil config to ${dest_dir}"
}

link_itermocil