#!/bin/bash

GLOBAL_NPM_MODULES="rimraf lerna commitizen cz-conventional-changelog react-native-cli react-native-git-upgrade create-react-app create-react-native-app typescript prettier"

install_nodejs() {
    read -p "Do you want to install all global npm modules? (Y/n) " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        npm install --global $GLOBAL_NPM_MODULES
    fi
}

install_nodejs