#!/bin/bash
WORKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Install Code Setting Sync
install_vscode_settings_sync() {
  local file_name="syncLocalSettings.json"
  local setting_file="$WORKDIR/$file_name"
  local dest_dir=$HOME/Library/Application\ Support/Code/User/
  local dest_file="$dest_dir/$file_name"
  if [ ! -f "${dest_file}" ]; then
    code --install-extension Shan.code-settings-sync
    sudo ln -s "${setting_file}" "${dest_file}"
    echo "Symlinked $setting_file to ${dest_file}"
  fi
}

enable_keyrepeat() {
  defaults write com.microsoft.VSCodeInsiders ApplePressAndHoldEnabled -bool false
}

copy_launch_file() {
  local file_name="launch.json"
  local launch_file="$WORKDIR/$file_name"
  cp launch_file ~/.vscode
}

install_vscode() {
  echo "[apps]: vscode is configuring..."
  install_vscode_settings_sync
  enable_keyrepeat
  copy_launch_file
  echo "VSCode has been configured!"
}

install_vscode