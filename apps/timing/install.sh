#!/bin/bash

# Active the Timing app licensing.
active_timing_license() {
    local mark_as_activated_file=$HOME/.devfor/.timing
    if [ ! -f $mark_as_activated_file ]; then
        if app_is_installed "Timing"; then
            open https://click.pstmrk.it/2sm/licensing.timingapp.com%2Factivate%2F2C77AE8F-8174-46CA-A8A5-74D6E6D2084E/8TExygE/7cQ/nWyFo0hO0L/bG9ja2Vy
            echo "activated" > $mark_as_activated_file
        fi
    fi
}

active_timing_license