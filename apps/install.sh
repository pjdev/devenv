#!/bin/bash

# Add an app name directory to here to install its scripts
# In each directory mush have at least one `install.sh` file to be installed.
declare -a APPS=("nodejs" "vim" "iterm" "itermocil" "vscode" "oh-my-zsh")

app_is_installed() {
  local app_name
  app_name=$(echo "$1" | cut -d'-' -f1)
  find /Applications -iname "$app_name*" -maxdepth 1 | egrep '.*' > /dev/null
}

install_user_scripts() {
    local workdir="$1"
    for i in "${APPS[@]}"
    do
        script_path="$workdir/$i/install.sh"
        echo "Installing $script_path"
        . $script_path
    done
}

install_user_scripts $1