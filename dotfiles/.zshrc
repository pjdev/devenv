# DECLARE
export ZSH="~/.oh-my-zsh"

# Important
#sudo spctl --master-disable

# THEME
ZSH_THEME="agnoster"
# ZSH_THEME="powerlevel9k/powerlevel9k"

# PLUGIN
plugins=(
  git command-not-found copydir copyfile cp history brew osx yarn zsh-autosuggestions
)

# COMMAND
#  git
alias gi="git init"
alias ginit="git init"

alias gc="git clone" #url
alias gcl="git clone" #url

alias gl="git log"
alias glog="git log"

alias gad="git add ."
alias gadd="git add ."

alias gcm="git commit -m"
alias gcmm="git commit -m" 
alias gcz="git cz"

alias grr="git remote rm origin"
alias gra="git remote add origin" #url
alias grao="git remote add origin" #url

## Git push using SSH
# git push --set-upstream git@gitlab.example.com:namespace/nonexistent-project.git master
## Git push using HTTP
# git push --set-upstream https://gitlab.example.com/namespace/nonexistent-project.git master

alias gpo="git push -u origin"
alias gpom="git push -u origin master" #Gratuitous Picture Of Myself.

alias gp="git pull"
alias gpu="git pull"
alias gpl="git pull"
alias gpull="git pull"
#  end git

# vc
alias c.="code ."
alias vsc="code ."
alias vc="code ."
alias vsc.="code ."

# node - npm
alias yc="yarn code"
alias y="yarn"

# source $ZSH/oh-my-zsh.sh
alias szsh="source ~/.zshrc"

# VIM
alias vczc="vim ~/.czcrc"
alias vzsh="vim ~/.npmrc"
alias vzsh="vim ~/.vimrc"
alias vzsh="vim ~/.zshrc"
alias vvsc="vim ~/.vscode/launch.json"

# CD
alias cdvim='cd ~/.vim_runtime'
alias cdomz="cd ~/.oh-my-zsh"

alias cdc='cd ~/codev'
alias cdcodev='cd ~/codev'

alias clc='cd ~/codev && ls -a'
alias clcodev='cd ~/codev && ls -a'

alias cdpj='cd ~/codev/pj'
alias clpj='cd ~/codev/pj && ls -a'

alias cddi='cd ~/codev/pj/devinit'
alias cddf='cd ~/codev/pj/devfor'
alias cdde='cd ~/codev/pj/devenv'

alias cdcode='cd ~/codev/pj/code'

# SSH
alias sshdev='ssh dev@127.0.0.1'

# VNC
alias vncdev='open vnc://name.duckdns.org:55555'

# itermocil
alias icodev='itermocil codev'

# SCRIPT
alias devfor='bash <(curl -s https://gitlab.com/pjdev/devfor/raw/master/scripts/init)'

# EXPORT
# export ANDROID_HOME=/usr/local/share/android-sdk
# export ANDROID_NDK_HOME=/usr/local/share/android-sdk/ndk-bundle
export ANDROID_HOME=~/Library/Android/sdk
export ANDROID_NDK_HOME=~/Library/Android/sdk/ndk-bundle

export MANPATH="/usr/local/man:$MANPATH"
export GIT_EDITOR=vim
export SSH_KEY_PATH="~/.ssh/rsa_id"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

PROMPT_TITLE='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/~}\007"'

export PATH="$HOME/.bin:$PATH"
export PROMPT_COMMAND="${PROMPT_TITLE}; ${PROMPT_COMMAND}"

# LANGUAGE
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# OTHER
# source
source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#export ZSH_HIGHLIGHT_HIGHLIGHTERS_DIR=/usr/local/share/zsh-syntax-highlighting/highlighters

compctl -g '~/.itermocil/*(:t:r)' itermocil

DEFAULT_USER="mac"

eval "$(hub alias -s)"

echo "ҟ"
